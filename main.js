// Build the chart
Highcharts.chart('container', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Мониторинг по операторам'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Цон Костанай',
            y: 40,
            sliced: true,
            selected: true,
            color: '#00A6ED'
        }, {
            name: 'Цон Костанай',
            y: 25,
            color: '#F6511D'
        },
        {
            name: 'Цон Костанай',
            y: 20
        },
        {
            name: 'Цон Костанай',
            y: 10
        },
        {
            name: 'Цон Костанай',
            y: 5,
        }]
    }]
});